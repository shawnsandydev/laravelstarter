<?php

namespace ShawnSandy\LaravelStarter;
use Illuminate\Support\Facades\Facade;

/**
 * Class PackageFacade
 *
 * @package \ShawnSandy\LaravelStarter
 */

class PackageFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        /*
         * The service container
         */
       return 'PACKAGE_ALIAS_NAME';
    }

}
