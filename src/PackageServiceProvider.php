<?php

namespace ShawnSandy\LaravelStarter;


use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Register views
         */
        $this->loadViewsFrom(__DIR__ . '/views', 'PACKAGE');

        /*
         * Publish Views
         */
        $this->publishes([
            //views
            __DIR__ . '/views' => resource_path('views/PACKAGE')
        ], 'views');

        /*
         * Publish config
         */
        $this->publishes([
            __DIR__ . '/config' => config_path('PACKAGE.php')
        ], 'config');

        /*
         * Assets
         */

        $this->publishes([
            __DIR__ . '/assets' => public_path('PACKAGE')
        ], 'public');


    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Bind the facade to to the class
         */
        $this->app->bind('PACKAGE_ALIAS_NAME', function () {
            return new \ShawnSandy\LaravelStarter\Package;
        });
    }
}
